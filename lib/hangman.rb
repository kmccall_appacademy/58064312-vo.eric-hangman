$words = File.readlines("lib/dictionary.txt")
$alphabet = ("A".."z").to_a

class Hangman
  $num_guesses = 0

  attr_reader :guesser, :referee, :board

  def initialize(player = {})
    @guesser = player[:guesser]
    @referee = player[:referee]
  end

  def setup
    length = @referee.pick_secret_word
    @guesser.register_secret_length(length)
    @board = Array.new(length)
  end

  def take_turn
    guess = @guesser.guess
    @referee.check_guess(guess)
    update_board
    @guesser.handle_response
  end

  def update_board
    @board
  end

end

class HumanPlayer
  def register_secret_length(length)
    length
  end
end

class ComputerPlayer
  attr_reader :candidate_words

  def initialize(dictionary)
    @dictionary = dictionary
  end

  def word
    @dictionary.sample
  end

  def pick_secret_word
    word.length
  end

  def register_secret_length(length)
    @candidate_words = @dictionary.select {|word| word.length == length}

  end

  def count(board)

    count = Hash.new(0)
    @candidate_words.each do |word|
      board.each_with_index do |char, i|
        count[word[i]] += 1 if char == nil
      end
    end
    count
  end


  def guess(board)
    count = count(board)
    common_letters = count.sort_by {|char, count| count}
    return common_letters.last[0]
  end

  def check_guess(guess)
    word.chars.each_index.select { |i| word[i] == guess }
  end

  def handle_response(guess, indices)
    @candidate_words.delete_if do |word|
      delete = false
      word.split("").each_with_index do |char, i|
        if char == guess && !indices.include?(i) ||
          char != guess && indices.include?(i)
          delete = true
          break
        end
      end
      delete
    end
  end

end
